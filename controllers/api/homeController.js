const { successResponse } = require('../../helpers/response');
const {
  Game, User, Score, City, History,
} = require('../../models');

class HomeController {
  getGames = (req, res) => {
    Game.findAll()
      .then((games) => {
        const game = games.map((oneGame) => ({
          id: oneGame.id,
          name: oneGame.name,
          description: oneGame.description,
          img_url: oneGame.img_url,
        }));
        res.json(successResponse(res, 200, game));
      })
      .catch((err) => {
        res.json({
          meta: { code: 422, message: 'empty game', err },
        });
      });
  };

  getDetailGame = (req, res) => {
    Game.findOne({
      where: { id: req.params.id },
    })
      .then((game) => {
        if (game) {
          res.json(
            successResponse(res, 200, {
              id: game.id,
              name: game.name,
              description: game.description,
              img_url: game.img_url,
            }),
          );
        } else {
          res.json(
            successResponse(res, 422, null, { message: 'game not found' }),
          );
        }
      })
      .catch((err) => {
        res.json({
          meta: { code: 422, message: 'game not found' },
        });
      });
  };

  leaderboard = (req, res) => {
    Score.findAndCountAll({
      where: {
        game_id: req.params.id,
      },
      include: [
        {
          model: User,
          as: 'user',
          include: [
            {
              model: City,
              as: 'city',
            },
          ],
        },
      ],
      order: [['score', 'DESC']],
      limit: 10,
    })
      .then((scores) => {
        const score = scores.rows.map((data) => ({
          id: data.id,
          score: data.score,
          user: {
            id: data.user.id,
            username: data.user.username,
            city: data.user.city,
          },
        }));
        res.json(successResponse(res, 200, scores));
      })
      .catch((err) => {
        res.json({
          meta: { code: 422, message: 'empty game', err },
        });
      });
  };

  createRoom = (req, res) => {
    const random = Math.random().toString(36).substr(2, 5);
    res.json(successResponse(res, 200, { room_id: random }));

    // History.create({ room_id: random })
    //   .then((room) => {
    //     res.json(successResponse(res, 200, { room_id: random }));
    //   })
    //   .catch((err) => {
    //     res.json({
    //       meta: { code: 422, message: 'Room Not Found', err },
    //     });
    //   });
  };

  playGame = (req, res) => {
    console.log(req.user);
    const currentUser = req.user;
    const { player_choice, room_id } = req.body;

    const getScore = (hasil) => {
      if (hasil == 'PLAYER WIN') {
        return 2;
      } if (hasil == 'COM WIN') {
        return 0;
      }
      return 1;
    };

    const comp_choice = () => {
      const comp = Math.random();
      if (comp < 0.34) return 'r';
      if (comp >= 0.34 && comp < 0.67) return 'p';
      return 's';
    };

    // rules Game
    // eslint-disable-next-line consistent-return
    const getHasil = (player, comp) => {
      if (player === comp) return 'DRAW';
      if (player === 'r') return comp === 'p' ? 'COM WIN' : 'PLAYER WIN';
      if (player === 'p') return comp === 's' ? 'COM WIN' : 'PLAYER WIN';
      if (player === 's') return comp === 'r' ? 'COM WIN' : 'PLAYER WIN';
    };

    History.findAll({ where: { room_id } }).then((user) => {
      // eslint-disable-next-line camelcase
      const computer_choice = comp_choice();
      const hasil = getHasil(player_choice, computer_choice);
      const lastUser = user.length === 0 ? 0 : user.slice(-1)[0].current_score;
      const lastScore = getScore(hasil) + lastUser;

      const round = user.length + 1;
      const next_round = round + 1;

      const saveHistory = () => {
        History.create({
          player_choice,
          computer_choice,
          result: hasil,
          round,
          current_score: lastScore,
          room_id,
          game_id: req.params.id,
          user_id: currentUser.id,
        })
          .then((data) => {
            res.json(
              successResponse(res, 200, {
                id: data.id,
                player_choice: data.player_choice,
                computer_choice: data.computer_choice,
                result: data.result,
                score: data.current_score,
                round: data.round,
                next_round: next_round === 4 ? null : next_round,
                room_id: data.room_id,
              }),
            );
          })
          .catch((err) => res.json(successResponse(res, 401, null, { message: err })));
      };

      if (user.length > 2) {
        res.json(
          successResponse(res, 200, null, {
            message: 'anda sudah bermain 3 ronde silahkan buat room',
          }),
        );
      } else if (user.length == 2) {
        saveHistory();
        Score.findOne({
          where: { user_id: req.user.id, game_id: req.params.id },
        }).then((data) => {
          if (!data) {
            Score.create({
              score: lastScore,
              game_id: req.params.id,
              user_id: req.user.id,
            });
          } else {
            Score.update(
              {
                score: data.score + lastScore,
              },
              {
                where: { user_id: req.user.id, game_id: req.params.id },
              },
            );
          }
        });
      } else {
        saveHistory();
      }
    });
  };
}

module.exports = HomeController;
