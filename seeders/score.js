'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Scores',
      [
        {
          score: 0,
          game_id: 1,
          user_id: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score: 0,
          game_id: 1,
          user_id: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score: 0,
          game_id: 1,
          user_id: 3,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score: 0,
          game_id: 1,
          user_id: 4,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score: 0,
          game_id: 1,
          user_id: 5,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score: 0,
          game_id: 1,
          user_id: 6,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score: 0,
          game_id: 1,
          user_id: 7,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score: 0,
          game_id: 1,
          user_id: 8,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score: 0,
          game_id: 1,
          user_id: 9,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score: 0,
          game_id: 1,
          user_id: 10,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score:0,
          game_id: 1,
          user_id: 11,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score: 1000,
          game_id: 2,
          user_id: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          score: 200,
          game_id: 2,
          user_id: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
