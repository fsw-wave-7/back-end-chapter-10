'use strict';
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.belongsTo(models.City, {
        foreignKey: 'city_id',
        as: 'city',
      });
      User.hasOne(models.Score, {
        foreignKey: 'user_id',
        as: 'score',
      });
    }
    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    static register = ({ email, username, password }) => {
      const encryptedPassword = this.#encrypt(password);
      return this.create({ email, username, password: encryptedPassword });
    };

    checkPassword = (password) => bcrypt.compareSync(password, this.password);

    static authenticate = async ({ email, password }) => {
      try {
        const user = await this.findOne({ where: { email } });
        if (!user) return Promise.reject('User not found!');
        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject('Wrong password');
        return Promise.resolve(user);
      } catch (err) {
        return Promise.reject(err);
      }
    };

    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username,
      };
      const rahasia = 'inirahasia';
      const token = jwt.sign(payload, rahasia);
      return token;
    };
  }
  User.init(
    {
      username: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      biodata: DataTypes.STRING,
      social_media_url: DataTypes.STRING,
      city_id: DataTypes.INTEGER,
      resetPasswordToken: DataTypes.STRING,
      resetPasswordExpires: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: 'User',
    }
  );
  return User;
};
