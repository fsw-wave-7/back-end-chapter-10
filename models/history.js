'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class History extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  History.init(
    {
      player_choice: DataTypes.STRING,
      computer_choice: DataTypes.STRING,
      result: DataTypes.STRING,
      round: DataTypes.STRING,
      current_score: DataTypes.INTEGER,
      room_id: DataTypes.STRING,
      user_id: DataTypes.INTEGER,
      game_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: 'History',
    }
  );
  return History;
};
