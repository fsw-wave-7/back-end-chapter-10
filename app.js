const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const port = process.env.port || 5000;
const cors = require("cors");
const passport = require("./lib/passport-jwt");
const app = express();
const { successResponse } = require("./helpers/response");

app.use(passport.initialize());
app.use(passport.session());

const api = require("./routes/api");
app.use(cors());

const swaggerJSON = require("./swagger.json");
const swaggerUI = require("swagger-ui-express");

app.use("/docs", swaggerUI.serve, swaggerUI.setup(swaggerJSON));

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/api", api);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.json(
    successResponse(res, 404, null, { message: "Salah alamat Boss!!!" })
  );
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

app.listen(process.env.PORT || 5000, () => {
  console.log(`Server is running on port ${port}, dude!`);
});

module.exports = app;
