var express = require('express');
var router = express.Router();
const AuthController = require('../controllers/api/authController');
const HomeController = require('../controllers/api/homeController');
const UserController = require('../controllers/api/userController');
const authController = new AuthController();
const homeController = new HomeController();
const userController = new UserController();
const { Pool } = require('pg');
const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
  ssl: {
    rejectUnauthorized: false,
  },
});
const restrict = require('../middleware/restrict-api');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

// USER
router.post('/register', authController.register);
router.post('/login', authController.login);

router.post('/logout', authController.logout);
router.post('/forgot', authController.forgot);
router.get('/reset/:token', authController.getReset);
router.post('/reset/:token', authController.postReset);

/// USER
// router.get('/users', userController.getAllUsers);
router.get('/profile', restrict, userController.getProfile);
router.put('/profile', restrict, userController.updateProfile);
router.get('/city', userController.getCity);

// HOME
router.get('/games', homeController.getGames);
router.get('/game/:id', homeController.getDetailGame);
router.get('/game/:id/score', homeController.leaderboard);
router.post('/game/:id/room', restrict, homeController.createRoom);

router.post('/game/:id/play', restrict, homeController.playGame);

router.get('/db', async (req, res) => {
  try {
    const client = await pool.connect();
    const result = await client.query('SELECT * FROM test_table');
    const results = { results: result ? result.rows : null };
    res.send('db');
    client.release();
  } catch (err) {
    console.error(err);
    res.send('Error ' + err);
  }
});

module.exports = router;
